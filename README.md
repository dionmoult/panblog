# panblog

Panblog is a minimalist blog system. You write articles in Markdown with
metadata in YAML, and Pandoc converts your articles to HTML. The entire system
of Panblog is a 73 line Makefile and 14 line lua filter, excluding HTML and Atom
feed templates.

Here's a [live demo of panblog](https://thinkmoult.com/).

## Features

 1. Open-sourced under the [MIT license](LICENSE).
 2. No frills. This README is probably as long as the code itself.
 3. Super-duper fast. Your generated blog is static HTML. No server language required.
 4. Plaintext post editing in Markdown.
 5. Supports showing a "Latest 10 posts" or similar on your homepage.
 6. Supports an archived list of all articles.
 7. Supports generation of Atom feeds.
 8. Custom HTML templates for fancy webdesigns with SEO optimisation and
    metatags.

## Installation

Ensure you have [Pandoc](https://pandoc.org/) and
[yq](https://mikefarah.github.io/yq/) installed.

 1. `git clone https://gitlab.com/dionmoult/panblog.git`
 4. `make all`
 5. Point your webroot at `weblog/`

## Global configuration

 - Modify `baseurl=https://yourblog.com/` in `Makefile`
 - Modify `templates/*.html5` to your blog design

## Write an article

 1. `vim articles/my-new-article/article.md`
 2. `vim articles/my-new-article/metadata.yaml`
 3. `make all`

A sample article is provided inside `articles/` if you get stuck.

## More things you can do

```
$ make articles # Regenerate all article HTML
$ make article name=my-article # Regenerate a single article
$ make archive # Update your article archive page
$ make index # Update your home page with 10 latest posts
$ make feed # Update your Atom feed
```
