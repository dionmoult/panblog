function Header(el)
    return el.level == 1 and {} or el
end

function Link(el)
    el.target = string.gsub(el.target, '%.%./articles/', '')
    el.target = string.gsub(el.target, '/article.md', '.html')
    if not string.match(el.target, '^http') then
        el.target = 'assets/' .. el.target
    end
    return el
end

function Image(el)
    if not string.match(el.src, '^http') then
        el.src = 'assets/' .. el.src
    end
    return el
end
