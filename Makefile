baseurl = /

.PHONY : all
all :
	make articles && make archive && make index && make feed

.PHONY : articles
articles :
	find articles/* -type d -exec sh -c 'make article name=`basename {}`' \;

.PHONY : article
article :
	mkdir -p weblog/assets/
	title=`head -n 1 articles/$$name/article.md | cut -c 3-`; \
	pandoc -s -f markdown-auto_identifiers -t html5 \
	--template=templates/article.html5 \
	--lua-filter=templates/filter.lua \
	--metadata-file=articles/$$name/metadata.yaml \
	--metadata=baseurl:$(baseurl) \
	--metadata=title:"$$title" \
	-o weblog/$$name.html articles/$$name/article.md
	cp -r articles/$$name/* weblog/assets/
	rm -f weblog/assets/article.md
	rm -f weblog/assets/metadata.yaml

.PHONY : feed
feed :
	timestamp=`date --rfc-3339=seconds | sed "s/ /T/"`; \
	echo "" | pandoc -s -t plain \
	--template=templates/feed.atom \
	--metadata-file=articles/archive.yaml \
	--metadata=baseurl:$(baseurl) \
	--metadata=timestamp:"$$timestamp" \
	-o weblog/feed.atom

.PHONY : index
index :
	printf "articles:\n" > articles/index.yaml
	for i in {0..19}; do \
		if [ `yq r articles/archive.yaml "articles[$$i].title"` == "null" ]; then break; fi; \
		yq w -i articles/index.yaml "articles[$$i].title" "`yq r articles/archive.yaml "articles[$$i].title"`"; \
		yq w -i articles/index.yaml "articles[$$i].slug" "`yq r articles/archive.yaml "articles[$$i].slug"`"; \
		yq w -i articles/index.yaml "articles[$$i].date" "`yq r articles/archive.yaml "articles[$$i].date"`"; \
		yq w -i articles/index.yaml "articles[$$i].description" "`yq r articles/archive.yaml "articles[$$i].description"`"; \
	done
	echo "" | pandoc -s -t html5 \
	--template=templates/index.html5 \
	--metadata-file=articles/index.yaml \
	--metadata=baseurl:$(baseurl) \
	-o weblog/index.html

.PHONY : archive
archive :
	printf "articles:\n" > articles/archive.yaml
	i=0 && for article in `find ./articles/ -type f -iname "article.md" -exec \
	sh -c 'echo \`yq r \\\`echo {} | sed -e "s/article.md/metadata.yaml/"\\\` date\` {}' \; \
	| sort -r | cut -c 12-`; do \
		metadata=`echo $$article | sed -e "s/article.md/metadata.yaml/"`; \
		title=`head -n 1 $$article | cut -c 3-`; \
		slug=`echo $$article | sed -e "s/article.md//" -e "s/\///g" | cut -c 10-`; \
		date=`yq r $$metadata date`; \
		description=`yq r $$metadata description`; \
		yq w -i articles/archive.yaml "articles[$$i].title" "$$title"; \
		yq w -i articles/archive.yaml "articles[$$i].slug" "$$slug"; \
		yq w -i articles/archive.yaml "articles[$$i].date" "$$date"; \
		yq w -i articles/archive.yaml "articles[$$i].description" "$$description"; \
		((i++)); \
	done;
	echo "" | pandoc -s -f markdown-auto_identifiers -t html5 \
	--template=templates/archive.html5 \
	--metadata-file=articles/archive.yaml \
	--metadata=baseurl:$(baseurl) \
	-o weblog/archive.html
